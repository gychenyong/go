// Package core
// @Author chenyong 2023/3/10 22:31:00
package core

import (
	"fmt"
	"gitee.com/gychenyong/go/core/app"
	"gitee.com/gychenyong/go/core/db"
	"gitee.com/gychenyong/go/core/logger"
	"gitee.com/gychenyong/go/core/valid"
	"gitee.com/gychenyong/go/core/viper"
	"gitee.com/gychenyong/go/global"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	enTranslations "github.com/go-playground/validator/v10/translations/en"
	zhTranslations "github.com/go-playground/validator/v10/translations/zh"
	"go.uber.org/zap"
	"reflect"
	"strings"
)

// InitViper
// @author:ChenYong
// @Description: 初始化viper，加载配置文件
// @param env 运行环境 dev test pro
// @param path 配置文件路径
func InitViper(env string, path ...string) {
	if len(path) == 0 {
		viper.Viper(env)
	} else {
		viper.Viper(env, path[0])
	}
}

// InitLogger
// @author:ChenYong
// @Description: 初始化日志并全局化
func InitLogger() {
	zap.ReplaceGlobals(logger.Logger())
	zap.RedirectStdLog(logger.Logger())
	global.LOG = zap.S()
}

// InitTrans
// @author:ChenYong
// @Description: 初始化翻译器
// @param locale
// @return err
func InitTrans(locale string) (err error) {
	// 修改gin框架中的Validator引擎属性，实现自定制
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {

		// 注册一个获取json tag的自定义方法
		v.RegisterTagNameFunc(func(fld reflect.StructField) string {
			name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
			if name == "-" {
				return ""
			}
			return name
		})

		zhT := zh.New() // 中文翻译器
		enT := en.New() // 英文翻译器

		// 第一个参数是备用（fallback）的语言环境
		// 后面的参数是应该支持的语言环境（支持多个）
		// uni := ut.New(zhT, zhT) 也是可以的
		uni := ut.New(enT, zhT, enT)

		// locale 通常取决于 http 请求头的 'Accept-Language'
		var ok bool
		// 也可以使用 uni.FindTranslator(...) 传入多个locale进行查找
		valid.Trans, ok = uni.GetTranslator(locale)
		if !ok {
			return fmt.Errorf("uni.GetTranslator(%s) failed", locale)
		}

		// 注册翻译器
		switch locale {
		case "en":
			err = enTranslations.RegisterDefaultTranslations(v, valid.Trans)
		case "zh":
			err = zhTranslations.RegisterDefaultTranslations(v, valid.Trans)
		default:
			err = enTranslations.RegisterDefaultTranslations(v, valid.Trans)
		}
		return
	}
	return
}

// InitDb
// @author:ChenYong
// @Description: 初始化数据库并产生数据库全局变量
// @return *gorm.DB
func InitDb() {
	switch global.CONFIG.System.DbType {
	case "mysql":
		global.DB = db.GormMysql()
	case "pgsql":
		global.DB = db.GormPgSql()
	default:
		global.DB = db.GormMysql()
	}
}

// InitHttp
// @author:ChenYong
// @Description: 初始化gin
func InitHttp() {
	app.HTTP = gin.Default()
}
