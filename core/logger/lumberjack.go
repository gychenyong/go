// Package logger
// @Author chenyong 2023/3/10 22:45:00
package logger

import (
	"gitee.com/gychenyong/go/global"
	"github.com/natefinch/lumberjack"
	"go.uber.org/zap/zapcore"
	"os"
	"path"
	"time"
)

var LumberJackLogs = new(lumberJackLogs)

type lumberJackLogs struct{}

func (l lumberJackLogs) GetWriteSyncer(level string) zapcore.WriteSyncer {
	fileWriter := &lumberjack.Logger{
		Filename:   path.Join(global.CONFIG.Log.Director, time.Now().Format("2006-1-2")+"-"+level+".log"),
		MaxSize:    global.CONFIG.Log.MaxSize,
		MaxBackups: global.CONFIG.Log.MaxBackup,
		MaxAge:     global.CONFIG.Log.MaxAge,
		Compress:   global.CONFIG.Log.Compress,
		LocalTime:  global.CONFIG.Log.LocalTime,
	}
	if global.CONFIG.Log.LogInConsole {
		return zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(fileWriter))
	}
	return zapcore.AddSync(fileWriter)
}
