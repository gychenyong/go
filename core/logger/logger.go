// Package logger
// @Author chenyong 2023/3/10 22:25:00
package logger

import (
	"fmt"
	"gitee.com/gychenyong/go/global"
	"gitee.com/gychenyong/go/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

// Zap 获取 zap.Logger
func Logger() (logger *zap.Logger) {
	if ok, _ := utils.PathExists(global.CONFIG.Log.Director); !ok { // 判断是否有Director文件夹
		fmt.Printf("create %v directory\n", global.CONFIG.Log.Director)
		_ = os.Mkdir(global.CONFIG.Log.Director, os.ModePerm)
	}

	cores := Zap.GetZapCores()
	logger = zap.New(zapcore.NewTee(cores...))

	if global.CONFIG.Log.ShowLine {
		logger = logger.WithOptions(zap.AddCaller())
	}
	return logger
}
