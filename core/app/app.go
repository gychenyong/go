// Package app
// @Author chenyong 2023/3/11 12:13:00
package app

import (
	"fmt"
	"gitee.com/gychenyong/go/global"
	"github.com/gin-gonic/gin"
	"os"
	"os/signal"
	"syscall"
)

var HTTP *gin.Engine

func Run() {
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGSEGV, syscall.SIGUSR1, syscall.SIGUSR2, os.Interrupt, os.Kill)
	go HTTP.Run(fmt.Sprintf(":%s", global.CONFIG.System.Port))
	<-c
}
