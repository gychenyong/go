// Package utils
// @Author chenyong 2023/3/11 10:47:00
package valid

import (
	ut "github.com/go-playground/universal-translator"
	"strings"
)

// 定义一个全局翻译器T
var Trans ut.Translator

func RemoveTopStruct(fields map[string]string) map[string]string {
	res := map[string]string{}
	for field, err := range fields {
		res[field[strings.Index(field, ".")+1:]] = err
	}
	return res
}
