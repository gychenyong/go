// Package route
// @Author chenyong 2023/3/11 12:52:00
package route

import "github.com/gin-gonic/gin"

var (
	GET  = "GET"
	POST = "POST"
)

// 路由分组定义
type RouteGroup struct {
	Name          string                     // 路由名称
	Prefix        string                     // 路由前缀
	Authorization bool                       // 是否需要认证
	Middleware    map[string]gin.HandlerFunc // 路由中间件
}

// 路由定义
type Route struct {
	Group      *RouteGroup                // 路由分组
	Action     string                     // 动作名称
	Path       string                     // 路由
	Method     string                     // 请求方法
	Middleware map[string]gin.HandlerFunc // 路由中间件
	Handler    gin.HandlerFunc
}
