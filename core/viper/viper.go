// Package core
// @Author chenyong 2023/3/10 22:11:00
package viper

import (
	"flag"
	"fmt"
	"gitee.com/gychenyong/go/global"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

// Viper
// @author:ChenYong
// @Description:
// @param env 运行环境 dev test pro
// @param path 配置文件路径
func Viper(env string, path ...string) {
	var config string

	if len(path) == 0 {
		flag.StringVar(&config, "c", "", "choose config file.")
		flag.Parse()
		if config == "" {
			if env == "dev" {
				config = "config_dev.yaml"
			} else if env == "test" {
				config = "config_test.yaml"
			} else {
				config = "config.yaml"
			}
			fmt.Printf("没有使用命令行,使用默认配置文件%s\n", config)
		} else { //命令行参数不为空，将命令行参数赋值给config
			fmt.Printf("您正在使用命令行的-c参数传递的值,config的路径为%s\n", config)
		}
	} else { // 函数传递的可变参数的第一个值赋值于config
		config = path[0]
		fmt.Printf("您正在使用func Viper()传递的值,config的路径为%s\n", config)
	}

	v := viper.New()

	v.SetConfigFile(config)
	v.SetConfigType("yaml")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	v.WatchConfig()

	v.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("config file changed:", e.Name)
		if err = v.Unmarshal(&global.CONFIG); err != nil {
			fmt.Println(err)
		}
	})
	if err = v.Unmarshal(&global.CONFIG); err != nil {
		fmt.Println(err)
	}
}
