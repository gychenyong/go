// @Author chenyong 2023/3/3 21:19:00
package utils

import (
	"bytes"
	"encoding/json"
)

func StructToJsonString(obj interface{}, pretty bool) string {
	marshal, _ := json.Marshal(obj)

	if pretty {
		// 美化
		var str bytes.Buffer
		_ = json.Indent(&str, marshal, "", "    ")
		return str.String()
	}

	return string(marshal)
}
