// @Author chenyong 2023/3/4 0:38:00
package utils

import (
	uuid "github.com/satori/go.uuid"
	"strings"
)

func UniqueString() string {
	return strings.Replace(uuid.NewV4().String(), "-", "", -1)
}
