// Package response
// @Author chenyong 2023/3/11 12:07:00
package response

type ResultCode string

const (
	SUCCESS ResultCode = "200"
	ERROR   ResultCode = "-1"

	PARAMS_IS_NULL      ResultCode = "100001"
	PARAMS_NOT_COMPLETE ResultCode = "100002"
	PARAMS_TYPE_ERROR   ResultCode = "100003"
	PARAMS_IS_INVALID   ResultCode = "100004"

	USER_NOT_EXIST         ResultCode = "200001"
	USER_NOT_LOGGED_IN     ResultCode = "200002"
	USER_ACCOUNT_ERROR     ResultCode = "200003"
	USER_ACCOUNT_FORBIDDEN ResultCode = "200004"
	USER_HAS_EXIST         ResultCode = "200005"

	BUSINESS_ERROR ResultCode = "300001"

	INTERFACE_INNER_INVOKE_ERROR ResultCode = "400001"
	INTERFACE_OUTER_INVOKE_ERROR ResultCode = "400002"
	INTERFACE_FORBIDDEN          ResultCode = "400003"
	INTERFACE_ADDRESS_INVALID    ResultCode = "400004"
	INTERFACE_REQUEST_TIMEOUT    ResultCode = "400005"
	INTERFACE_EXCEED_LOAD        ResultCode = "400006"

	FAILED ResultCode = "500000"

	DATA_NOT_FOUND       ResultCode = "600001"
	DATA_IS_WRONG        ResultCode = "600002"
	DATA_ALREADY_EXISTED ResultCode = "600003"

	PERMISSION_NO_ACCESS ResultCode = "700001"
)
