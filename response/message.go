// Package response
// @Author chenyong 2023/3/11 12:07:00
package response

var ResultMessage = map[ResultCode]string{
	SUCCESS: "操作成功",
	ERROR:   "系统繁忙，请稍后重试",

	PARAMS_IS_NULL:      "参数为空",
	PARAMS_NOT_COMPLETE: "参数不全",
	PARAMS_TYPE_ERROR:   "参数类型错误",
	PARAMS_IS_INVALID:   "参数无效",

	// 用户错误
	USER_NOT_EXIST:         "用户不存在",
	USER_NOT_LOGGED_IN:     "用户未登陆",
	USER_ACCOUNT_ERROR:     "用户名或密码错误",
	USER_ACCOUNT_FORBIDDEN: "用户账户已被禁用",
	USER_HAS_EXIST:         "用户已存在",

	// 业务错误
	BUSINESS_ERROR: "系统业务出现问题",

	// 接口错误
	INTERFACE_INNER_INVOKE_ERROR: "系统内部接口调用异常",
	INTERFACE_OUTER_INVOKE_ERROR: "系统外部接口调用异常",
	INTERFACE_FORBIDDEN:          "接口禁止访问",
	INTERFACE_ADDRESS_INVALID:    "接口地址无效",
	INTERFACE_REQUEST_TIMEOUT:    "接口请求超时",
	INTERFACE_EXCEED_LOAD:        "接口负载过高",

	// 系统错误
	FAILED: "系统内部错误",

	// 数据错误
	DATA_NOT_FOUND:       "数据未找到",
	DATA_IS_WRONG:        "数据有误",
	DATA_ALREADY_EXISTED: "数据已存在",

	// 权限错误
	PERMISSION_NO_ACCESS: "没有访问权限",
}
