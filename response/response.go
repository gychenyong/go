// Package response
// @Author chenyong 2023/3/11 12:08:00
package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Response struct {
	Code    ResultCode  `json:"code"`
	Message string      `json:"message"`
	Payload interface{} `json:"payload"`
}

func Result(c *gin.Context, code ResultCode, data interface{}, message ...string) {
	if len(message) == 0 {
		c.JSON(http.StatusOK, Response{
			code,
			ResultMessage[code],
			data,
		})
	} else {
		c.JSON(http.StatusOK, Response{
			code,
			message[0],
			data,
		})
	}
}

func Ok(c *gin.Context) {
	Result(c, SUCCESS, nil)
}

func OkWithMessage(c *gin.Context, message string) {
	Result(c, SUCCESS, nil, message)
}

func OkWithDetaile(c *gin.Context, code ResultCode, data interface{}, message ...string) {

	if len(message) == 0 {
		Result(c, code, data)
	} else {
		Result(c, code, data, message[0])
	}
}

func Fail(c *gin.Context) {
	Result(c, FAILED, nil)
}

func FailWithMessage(c *gin.Context, message string) {
	Result(c, FAILED, nil, message)
}

func FailWithDetaile(c *gin.Context, code ResultCode, data interface{}, message ...string) {

	if len(message) == 0 {
		Result(c, code, data)
	} else {
		Result(c, code, data, message[0])
	}
}
